# pylint: disable=E0401
# pylint: disable=C0301
# Works with Python 3.6
import discord
from discord.ext import commands

client = commands.Bot(command_prefix="!")  # Defines the bot with the prefix "!"
token = "bot token"  # Token to

@client.event
async def on_ready():  # Once the bot is ready:
    print("The bot is ready!")  # Notifies the console that the bot is ready
    await client.change_presence(game=discord.Game(name="Slippery Slope"))  # Changes the game status of the bot to whatever is inside name=""

@client.event
async def on_message(message):
    # Ensures that the bot doesn't reply to itself
    if message.author == client.user:
        return

    # Fun Dad jokes are always fun, and are needed in every bot.
    dad_initiation = ["im", "IM", "iM", "Im", "i'M", "I'M", "I'm", "i'm", "i\"M", "I\"M", "I\"m", "i\"m"]
    for sentence in message.content.split("."):  # For each sentence in the message, split along ".":
        for word in sentence.split():  # For each word in the sentence:
            if word in dad_initiation:  # If the word is part of the dad joke:
                sentence = sentence.split(word)  # Split the sentence by the word
                sentence = str(sentence[1].replace("{} ".format(word), ""))[1:]  # Only keep the stuff after the dad_initiation, and get rid of it.
                await client.send_message(message.channel, "Hi \"{}\", I'm dad!".format(sentence))  # Send the message back to the same channel it was called on, while reformated.

    # Requested by one of our developers
    if "69" in message.content:  # If the phrase "69" in the message:
        await client.send_message(message.channel, "nice")  # Outputs "nice"

    await client.process_commands(message)  # Allow commands to be called from a user (without this line the bot won't work at all)

@client.command()
async def ping():  # Creates a command called ping
    await client.say("Pong!")  # Sends a reply message "Pong!"

@client.command()
async def echo(*args):  # Creates a command called echo that takes in infinite arguments
    output = ""  # Sets an output variable to be added to
    for word in args:  # For every word that is passed through the arguments of the command:
        output += "{} ".format(word)  # Add that word to the output variable
    await client.say(output)  # Sends the total output back to the channel, echoing whatever the user wanted.

@client.command(pass_context=True)  # pass_context means that the code can get the author of whoever called the command.
async def startdm(ctx):  # Creates a command called startdm
    author = ctx.message.author  # Sets a variable to the author of the command caller.
    await client.send_message(author, "You have started a DM with PrivCord.")  # Sends a message to the author's DMs.

client.run(token)
