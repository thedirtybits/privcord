# **PrivCord - Discord Encryption Done Right**

PrivCord is a Discord bot that takes encryption to a new level. It uses OpenPGP to encrypt messages between users,
and exchanges private keys through DMs.

## How Does it Work?

Let's say you have two users. User #1 wants to send an encrypted message to User #2.
User #1 first uploads their message to pastebin, and sends a link to the bot.
User #1 then tells the bot which user to send the message to. In this case, that would be User #2.
The bot sends the encrypted message to User #2 in the server where User #1 called the bot, and sends the public key to User #2.
From there, User #2 can decrypt the message with the public key given by the bot.