# pylint: disable=E0401
from pbwrap import Pastebin
def encrypted_paste(path_to_file):
    '''
    Takes a file path as input and makes a paste with it's contents.
    It returns the link to the paste.
    '''
    if path_to_file == '':
        return 'ERROR: No file specified'
    try:
        a = open(path_to_file, 'r')
    except FileNotFoundError:
        return f'ERROR: File not found - {path_to_file}'
        exit()
    a.close()
    pb = Pastebin('api key')
    pb.authenticate('user', 'password')
    link = pb.create_paste_from_file(path_to_file, 2, 'a', 'N', 'text')
    return link
