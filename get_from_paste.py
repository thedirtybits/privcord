import subprocess, re
def get_from_paste(link):
    '''
    Takes a Pastebin link as input (i.e https://pastebin.com/Xgt3Tjrm or
    https://pastebin.com/raw/Xgt3Tjrm) and puts the contents into a file
    named data(paste_ID).txt
    '''
    if '/raw/' not in link: # If the link is not just the raw data
        try:
            pid = re.search('.*/(.*)', link).group(1) # Get paste ID
        except AttributeError:
            return f'ERROR: Invalid link'
        link = f'https://pastebin.com/raw/{pid}'
    raw_paste_data = subprocess.check_output(['curl', '-s', link])
    if raw_paste_data.decode() == 'Error with this ID!' or raw_paste_data.decode() == '':
        return f'ERROR: Invalid link'
    with open(f'data{pid}.txt', 'wb') as raw_data:
        raw_data.write(raw_paste_data)
    return raw_paste_data.decode()
